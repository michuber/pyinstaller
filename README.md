# pyinstaller

Copy of [cdrx/docker-pyinstaller](https://github.com/cdrx/docker-pyinstaller) for Python 3.9.9

## Docker command

Running `pyinstaller --clean -y --dist ./dist/windows --workpath /tmp *.spec` in current directory:
```
docker run -it --rm --volume ${PWD}:/src registry.gitlab.com/michuber/pyinstaller:windows
```

## `.gitlab-ci.yml`

Example for GitLab-CI to build Windows program:

```
build-windows-exe:
  stage: build
  image:
    name: registry.gitlab.com/michuber/pyinstaller:windows
    entrypoint: [""]
  variables:
    SRCDIR: '/builds/${CI_PROJECT_PATH}'
  script:
    - 'echo "workdir: $(pwd)"'
    - 'ls -l'
    - '/entrypoint.sh pyinstaller --clean -y --dist ./dist/windows --workpath /tmp --onefile main.spec'
  artifacts:
    paths:
      - dist/windows
  tags:
    - docker
```
